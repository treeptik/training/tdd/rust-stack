# TDD Rust Stack

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [License](LICENSE.md).

## From Scratch

```shell
cargo new --lib stack

cd stack

wget -qO.gitignore https://gitignore.io/api/git,rust,vim,linux,macos,windows
```

## Running Tests

```shell
cargo test
```
