pub struct Stack<T> {
    contents: Vec<T>
}

impl<T> Stack<T> {
    pub fn new() -> Self {
        Stack { contents: Vec::new() }
    }

    pub fn is_empty(&self) -> bool {
        self.contents.is_empty()
    }

    pub fn push(&mut self, x: T) {
        self.contents.push(x);
    }

    pub fn pop(&mut self) -> Option<T> {
        self.contents.pop()
    }
}

#[cfg(test)]
mod tests {
    pub use super::*;

    mod given_empty_stack {
        pub use super::*;

        fn given() -> Stack<i64> {
            Stack::new()
        }

        #[test]
        fn then_is_empty() {
            let stack = given();

            assert!(stack.is_empty());
        }

        #[test]
        fn then_pop_returns_nothing() {
            let mut stack = given();

            assert_eq!(None, stack.pop());
        }

        mod when_pushing {
            use super::*;

            fn when() -> Stack<i64> {
                let mut stack = given();
                stack.push(1);
                stack
            }

            #[test]
            fn then_not_empty() {
                let stack = when();

                assert!(!stack.is_empty());
            }

            #[test]
            fn then_pop_returns_element() {
                let mut stack = when();

                assert_eq!(Some(1), stack.pop());
            }
        }
    }

    mod given_singleton_stack {
        pub use super::*;

        fn given() -> Stack<i64> {
            let mut stack = Stack::new();
            stack.push(2);
            stack
        }

        mod when_popping {
            use super::*;

            fn when() -> Stack<i64> {
                let mut stack = given();
                stack.pop();
                stack
            }

            #[test]
            fn then_is_empty() {
                let stack = when();

                assert!(stack.is_empty());
            }
        }
    }

    mod given_nonempty_stack {
        pub use super::*;

        fn given() -> Stack<i64> {
            let mut stack = Stack::new();
            stack.push(1);
            stack.push(2);
            stack.push(3);
            stack
        }

        mod when_pushing {
            use super::*;

            fn when() -> Stack<i64> {
                let mut stack = given();
                stack.push(4);
                stack
            }

            #[test]
            fn then_not_empty() {
                let stack = when();

                assert!(!stack.is_empty());
            }

            #[test]
            fn then_pop_returns_element() {
                let mut stack = when();

                assert_eq!(Some(4), stack.pop());
            }

            #[test]
            fn then_former_elements_remain() {
                let mut stack = when();

                stack.pop();
                assert_eq!(Some(3), stack.pop());
                assert_eq!(Some(2), stack.pop());
                assert_eq!(Some(1), stack.pop());
                assert_eq!(None, stack.pop());
            }
        }
    }
}
